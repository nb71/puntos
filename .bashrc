#
# $Id: .bashrc,v 1.8 2005/11/15 11:41:50 
#
# Este archivo se ejecuta cuando el usuario se loguea o abre
# una nueva terminal virtual
#

#####################################################################
###########   SETEO DE VARIABLES Y ALIASES GENERALES   ##############
#####################################################################

#echo entro en .bashrc
#echo PATH = $PATH

# Ejecuta la conf general del sistema
if [ -f /etc/bashrc ]; then
	 source /etc/bashrc
fi

#if ! { typeset -f setenv > /dev/null } ; then
	# para compatibilidad csh
	function setenv(){
		export $1="$2"
	}
	export -f setenv
#fi

pathmunge () {
	if [ -d $1 ] && ! echo $PATH | /bin/egrep -q "(^|:)$1($|:)"
	then
		if [ "$2" = "after" ]
		then
			PATH=$PATH:$1
		else
			PATH=$1:$PATH
		fi
	fi
}

# no queremos saber a cada minuto que tenemos mail en algun lado
unset MAILCHECK

#PERTLEOH: 20050113 los tiempos cambian, si se cae una conexion por error
#          se levanta y se sigue adelante y nadie sufre demasiado
#set	-o ignoreeof		# don't let control-d logout

#setenv EXINIT 'set ts=4 |syn on| set showmode | set ai |set ignorecase | set redraw | set showmatch | set noslow | set autoprint | set report=5 | set noterse | set wm=8 |set ai | map     :set wrapmargin=0 | map     {!}sort -b | map T :r! | map! [M lD+10li'

# Configuracion de alias y funciones Generales.

if which vim >/dev/null 2>&1 ; then
	alias vi="vim"
fi

alias gerp='grep'
alias dri=dir
alias mroe=more
alias moer=more
alias lp='lp -o nobanner'
alias borrar='/bin/rm'
alias cdfold='cd ~/mailer'
alias dir='ls -laF --color=auto'
alias h='history|tail -100'
alias lo='exit'
alias ls='ls -F --color=auto'
alias lc='ls -a --color=auto'
alias l='ls -laF --color=auto'
alias j='jobs -l'
alias kj='kill %%'
alias more=less
alias md='mkdir'
alias rd='rmdir'
alias rm='/bin/rm -i'
alias acentos='xmodmap ~/.xmodmap_es 2> /dev/null'
alias sin_acentos='xmodmap ~/.xmodmap_us 2> /dev/null'
alias musica='smbmount //cosa/musica /mnt/cosa -o username=edgardo'
ccc () {
cc 2>&1 $*.c -o $* |  less
}
gtarc () {
tar cf - $* | gzip > $*.tar.gz 
}
gtart () {
gzip  -c -d `basename $1 .tar.gz`.tar.gz | tar tvf - 
}
gtarx () {
gzip  -c -d `basename $1 .tar.gz`.tar.gz | tar xvf - 
}
ctarc () {
tar cf - $* | compress > $*.tar.Z 
}
ctart () {
compress  -c -d `basename $1 .tar.Z`.tar.Z | tar tvf - 
}
ctarx () {
compress  -c -d `basename $1 .tar.Z`.tar.Z | tar xvf - 
}
ctarcen () {
tar cf - $* | compress |uuencode $*.tar.Z > $*.uu
}
ctarcl () {
tar cf - $* |li|compress > $*.Z
}
ctarlt () {
uncompress < $*.Z|li|tar tvf - 
}
ctarx1 () {
compress  -c -d $1.tar.Z | tar xvf - $2
}
ctarxde () {
uudecode $*.uu;compress  -c -d $*.tar.Z| tar xvf - 
}
ctarxl () {
uncompress < $*.Z|li|tar xf - 
}
ff () {
find . -name \*$*\* -print
}
nd () {
mkdir $1 ; cd $1
}
vigrep () {
vi `grep -l $1 $*`
}
tn () {
toolwait xterm -geometry +580+555 -T $1 -n $1 -e telnet $1 &
}
bh () {
HISTSIZE=0
}
rmbh () {
HISTSIZE=0;rm ~/.bash_history ~system/.bash_history
}
inspath () 
	{ 
		setenv PATH $1:$PATH
	}
lt() {
ls -lt $1|head
}

# ejecuta la configuracion especifica segun OS
if [ -f ~/.$OSTYPE ]; then
	source ~/.$OSTYPE
fi

# ejecuta la configuracion especifica del equipo
# por ejemplo variables de oracle, o alias especificos
if [ -f ~/.$HOSTNAME ]; then
	source ~/.$HOSTNAME
fi


#Seteo de prompt
t=`tty|sed 's/tty//'`
ter=`basename $t`
unset t

PS1="[\u@\h:\W $ter]\\$ "

# Ejecuta la conf particular del usuario
if [ -f ~/.bashrc.$USER ]; then
         source ~/.bashrc.$USER
fi


## CVS
if [ -f ~/devel/develtools/cvsenv ]; then
	PATH="$PATH:$HOME/devel/develtools"
	. ~/devel/develtools/cvsenv
fi

#echo salgo de .bashrc
#echo PATH = $PATH


