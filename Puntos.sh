function setenv(){
                export $1="$2"
        }
        export -f setenv

setenv LESS M
setenv PAGER less
setenv MPAGE "-X -of2bA4"

if which vim >/dev/null 2>&1 ; then
        alias vi="vim"
fi

alias gerp='grep'
alias dri=dir
alias mroe=more
alias moer=more
alias lp='lp -o nobanner'
alias borrar='/bin/rm'
alias cdfold='cd ~/mailer'
alias dir='ls -laF --color=auto'
alias h='history|tail -100'
alias lo='exit'
alias ls='ls -F --color=auto'
alias lc='ls -a --color=auto'
alias l='ls -laF --color=auto'
alias j='jobs -l'
alias kj='kill %%'
alias more=less
alias md='mkdir'
alias rd='rmdir'
alias rm='/bin/rm -i'
alias acentos='xmodmap ~/.xmodmap_es 2> /dev/null'
alias sin_acentos='xmodmap ~/.xmodmap_us 2> /dev/null'
alias musica='smbmount //cosa/musica /mnt/cosa -o username=edgardo'
ccc () {
cc 2>&1 $*.c -o $* |  less
}
gtarc () {
tar cf - $* | gzip > $*.tar.gz 
}
gtart () {
gzip  -c -d `basename $1 .tar.gz`.tar.gz | tar tvf - 
}
gtarx () {
gzip  -c -d `basename $1 .tar.gz`.tar.gz | tar xvf - 
}
ctarc () {
tar cf - $* | compress > $*.tar.Z 
}
ctart () {
compress  -c -d `basename $1 .tar.Z`.tar.Z | tar tvf - 
}
ctarx () {
compress  -c -d `basename $1 .tar.Z`.tar.Z | tar xvf - 
}
ctarcen () {
tar cf - $* | compress |uuencode $*.tar.Z > $*.uu
}
ctarcl () {
tar cf - $* |li|compress > $*.Z
}
ctarlt () {
uncompress < $*.Z|li|tar tvf - 
}
ctarx1 () {
compress  -c -d $1.tar.Z | tar xvf - $2
}
ctarxde () {
uudecode $*.uu;compress  -c -d $*.tar.Z| tar xvf - 
}
ctarxl () {
uncompress < $*.Z|li|tar xf - 
}
ff () {
find . -name \*$*\* -print
}
nd () {
mkdir $1 ; cd $1
}
vigrep () {
vi `grep -l $1 $*`
}
tn () {
toolwait xterm -geometry +580+555 -T $1 -n $1 -e telnet $1 &
}
bh () {
HISTSIZE=0
}
rmbh () {
HISTSIZE=0;rm ~/.bash_history ~system/.bash_history
}
inspath () 
        { 
                setenv PATH $1:$PATH
        }
lt() {
ls -lt $1|head
}


mpman () {
nroff -man $* | sed 's///g' | mpage | lpr
}

alias prs='ps -ef '
psg () {
ps -ef|grep -i $*|grep -v grep
}
psk () {
j=`ps auxw|grep -i $*| grep -v grep | head -1`;echo $j;kill `echo $j|cut -f2 -d" "`
}
psk9 () {
j=`ps auxw|grep -i $*| grep -v grep | head -1`;echo $j;kill -9 `echo $j|cut -f2 -d" "`
}
