#
# $Id: .bash_profile,v 1.4 2005/12/01 15:23:20 
#
# Este archivo se ejecuta cuando el usuario se loguea
#
#echo entro en .bash_profile
#echo PATH = $PATH
#set -x

# para compatibilidad csh
#if ! { typeset -f setenv > /dev/null } 
#then
	function setenv(){
		export $1="$2"
	}
	export -f setenv
#fi
#stty intr ^Y
stty erase ^?

# Configuracion de variables de otros softs...
setenv LESS M
setenv PAGER less
setenv MPAGE "-X -of2bA4"

# directory search path for cd
setenv CDPATH .:~ 


# Corre el .bashrc
if [ -f ~/.bashrc ]; then
  source ~/.bashrc
fi

# Configuracion de PATHS segun OSTYPE
# echo $PATH
case $OSTYPE in
sunos)
	pathmunge /usr/snm/bin after
	pathmunge /l/iso/bin after
	;;
solaris)
	pathmunge /opt/SUNWconn/bin after
	;;
Linux)
	pathmunge /sbin after
	pathmunge /usr/sbin after
	pathmunge /usr/X11R6/bin after
	pathmunge /usr/local/bin after
	;;
linux-gnu)
	pathmunge /usr/local/sbin after
	pathmunge /usr/sbin after
	pathmunge /sbin after
	pathmunge /usr/local/bin
	;;
esac

pathmunge ~/bin after

export PATH

if [ -f ~/.local.sh ]; then
  source ~/.local.sh
fi


#echo salgo de  .bash_profile
#echo PATH = $PATH
